#!/usr/bin/env bats

setup() {
    IMAGE_NAME="test_docker"
    CONTAINER_NAME="${IMAGE_NAME}_container"
}

@test "image build" {
    docker build -t $IMAGE_NAME $BATS_TEST_DIRNAME/../client

    [ "$?" -eq 0 ]
}

@test "can pull buyme image" {
    docker run --rm -v /var/run/docker.sock:/var/run/docker.sock $IMAGE_NAME pull dcr.buyme360.com/devops/roundcube:6a68ee083e98775e1eb972c530f29fdc9ee34aa6
}
